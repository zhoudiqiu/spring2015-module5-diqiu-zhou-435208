function fetchWeather(event){
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET","weather_json.php",true);
    xmlHttp.addEventListener("load",weatherCallback,false);
    xmlHttp.send(null);
}
function weatherCallback(event){
    //pasing the JSON data 
    var jsonData=JSON.parse(event.target.responseText);
    var city=jsonData.location.city;
    var state=jsonData.location.state;
    var humidity=jsonData.atmosphere.humidity;
    var temperature=jsonData.current.temp;
    var tomorrow=jsonData.tomorrow.code;
    var dayafter=jsonData.dayafter.code;

    var xmlHttp = new XMLHttpRequest();
        xmlHttp.open("GET","weather.html",true);
        xmlHttp.addEventListener("load", 
function(event){
    var cityO=document.createElement("strong");
    var locO=document.getElementsByClassName("weather-loc")[0];
    var humidityO=document.getElementsByClassName("weather-humidity")[0];
    var tempO=document.getElementsByClassName("weather-temp")[0];
    var tomorrowO=document.getElementsByClassName("weather-tomorrow")[0];
    var dayAfterO=document.getElementsByClassName("weather-dayaftertomorrow")[0];
                
    cityO.innerHTML=city;
    locO.innerHTML="<Strong>"+city+"</Strong> "+state; 
    humidityO.innerHTML=humidity;
    tempO.innerHTML=temperature;
    tomorrowO.setAttribute("src",'http://us.yimg.com/i/us/nws/weather/gr/'+tomorrow+'ds.png');
    dayAfterO.setAttribute("src",'http://us.yimg.com/i/us/nws/weather/gr/'+dayafter+'ds.png');
        },false);
    xmlHttp.send(null);
}

                //bind fetchWeather() to the DOMContentLoaded event
document.addEventListener("DOMContentLoaded", fetchWeather, false);
document.getElementById("update").addEventListener("click",fetchWeather,false);